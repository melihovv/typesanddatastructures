#ifndef STACKOFBOOKS_BOOK_H
#define STACKOFBOOKS_BOOK_H

#include <string>

class Book
{
public:
    Book(const std::string &name);
    Book(const Book &book);

    bool operator==(const Book &book) const;

    std::string getName() const
    {
        return name;
    }

private:
    std::string name;
};

#endif //STACKOFBOOKS_BOOK_H
