#include "MyDoubleLinkedList.h"

MyDoubleLinkedList::MyDoubleLinkedList(int rotateCount)
{
    this->rotateCount = rotateCount;
}

MyDoubleLinkedList::~MyDoubleLinkedList()
{
    clear();
}

void MyDoubleLinkedList::append(const Book &book)
{
    ListNode *node = new ListNode(book);
    node->up = nullptr;

    // Список не пуст.
    if (bottom)
    {
        node->down = top;
        top->up = node;
        top = node;
    }
    else
    {
        node->down = nullptr;
        bottom = top = node;
    }

    ++length;
}

void MyDoubleLinkedList::push_front(const Book &book)
{
    ListNode *node = new ListNode(book);
    node->down = nullptr;

    if (bottom)
    {
        node->up = bottom;
        bottom->down = node;
        bottom = node;
    }
    else
    {
        node->up = nullptr;
        bottom = top = node;
    }

    ++length;
}

void MyDoubleLinkedList::rotate()
{
    ListNode *start = top;
    for (int i = 0; i < rotateCount - 1 && start->down; ++i)
    {
        start = start->down;
    }

    ListNode *theMostBottomNodeToRotate = start;

    while (start)
    {
        std::swap(start->up, start->down);
        start = start->down;
    }

    if (rotateCount < length)
    {
        theMostBottomNodeToRotate->up->up = top;
        top->down = theMostBottomNodeToRotate->up;
        theMostBottomNodeToRotate->up = nullptr;
        top = theMostBottomNodeToRotate;
    }
    else
    {
        std::swap(bottom, top);
    }
}

void MyDoubleLinkedList::print()
{
    ListNode *node = top;
    while (node)
    {
        std::cout << node->getBook().getName() << std::endl;
        node = node->down;
    }
}

void MyDoubleLinkedList::clear()
{
    while (bottom)
    {
        top = bottom->up;
        delete bottom;
        bottom = top;
    }

    length = 0;
}
