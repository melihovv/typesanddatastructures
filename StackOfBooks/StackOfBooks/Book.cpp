#include "Book.h"

Book::Book(const std::string &name)
{
    this->name = name;
}

Book::Book(const Book &book)
{
    this->name = book.name;
}

bool Book::operator==(const Book &book) const
{
    return this->name == book.name;
}
