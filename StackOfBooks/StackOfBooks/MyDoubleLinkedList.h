#ifndef STACKOFBOOKS_MYDOUBLELINKEDLIST_H
#define STACKOFBOOKS_MYDOUBLELINKEDLIST_H

#include <iostream>
#include "ListNode.h"

class MyDoubleLinkedList {
public:
    MyDoubleLinkedList(int rotateCount);
    ~MyDoubleLinkedList();

    void append(const Book &book);
    void push_front(const Book &book);
    void rotate();
    void print();
    void clear();

    ListNode *bottom = nullptr;
    ListNode *top = nullptr;

private:
    int rotateCount = 0;
    int length = 0;
    static const int MAX_BOOK_COUNT = 40000;
};

#endif //STACKOFBOOKS_MYDOUBLELINKEDLIST_H
