#ifndef STACKOFBOOKS_LISTNODE_H
#define STACKOFBOOKS_LISTNODE_H

#include "Book.h"

class ListNode
{
public:
    ListNode(const Book &book);

    Book getBook() const
    {
        return book;
    }

    ListNode *down;
    ListNode *up;

private:
    Book book;
};

#endif //STACKOFBOOKS_LISTNODE_H
