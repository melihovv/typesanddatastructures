#include <iostream>
#include <string>
#include <regex>
#include "MyDoubleLinkedList.h"

int main()
{
    int startBooksCount = 0;
    int commandsCount = 0;
    int rotateCount = 0;
    std::cin >> startBooksCount;
    std::cin >> commandsCount;
    std::cin >> rotateCount;

    if (startBooksCount < 0 || startBooksCount > 40000 ||
        commandsCount < 0 || commandsCount > 100000 ||
        rotateCount < 0 || rotateCount > 40000)
    {
        std::cout << "Invalid argument(s)" << std::endl;
    }

    MyDoubleLinkedList list(rotateCount);

    for (int i = 0; i < startBooksCount; ++i)
    {
        std::string temp;
        std::cin >> temp;
        list.push_front(temp);
    }

    for (int j = 0; j < commandsCount; ++j)
    {
        std::string operation;
        std::cin >> operation;

        std::regex addCommand("ADD\\(([A-Z]{1,3})\\)");
        std::regex rotateCommand("ROTATE");
        std::smatch matched;

        if (std::regex_search(operation, matched, addCommand))
        {
            list.append(matched[1].str());
        }
        else if (std::regex_match(operation, rotateCommand))
        {
            list.rotate();
        }
    }

    list.print();

    return 0;
}
