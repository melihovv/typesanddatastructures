#include <gtest\gtest.h>
#include "../StackOfBooks/MyDoubleLinkedList.h"

TEST(MyDoubleLinkedList, append)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(2);
    list.append(Book("ABC"));
    list.append(Book("ABD"));
    list.append(Book("ABE"));

    ASSERT_EQ(list.bottom->getBook(), Book("ABC"));
    ASSERT_EQ(list.bottom->down, nullptr);
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABD"));

    ASSERT_EQ(list.top->down->getBook(), Book("ABD"));
    ASSERT_EQ(list.top->getBook(), Book("ABE"));
    ASSERT_EQ(list.top->up, nullptr);

    list.clear();
}

TEST(MyDoubleLinkedList, push_front)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(2);
    list.push_front(Book("ABC"));
    list.push_front(Book("ABD"));
    list.push_front(Book("ABE"));

    ASSERT_EQ(list.bottom->getBook(), Book("ABE"));
    ASSERT_EQ(list.bottom->down, nullptr);
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABD"));

    ASSERT_EQ(list.top->down->getBook(), Book("ABD"));
    ASSERT_EQ(list.top->getBook(), Book("ABC"));
    ASSERT_EQ(list.top->up, nullptr);

    list.clear();
}

TEST(MyDoubleLinkedList, rotate)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(2);
    list.append(Book("ABC"));
    list.append(Book("ABD"));
    list.append(Book("ABE"));
    list.rotate();

    ASSERT_EQ(list.bottom->getBook(), Book("ABC"));
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABE"));
    ASSERT_EQ(list.top->getBook(), Book("ABD"));

    list.clear();
}

TEST(MyDoubleLinkedList, rotate_non_rotating_more_than_one_book)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(2);
    list.append(Book("ABC"));
    list.append(Book("ABF"));
    list.append(Book("ABD"));
    list.append(Book("ABE"));
    list.rotate();

    ASSERT_EQ(list.bottom->getBook(), Book("ABC"));
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABF"));
    ASSERT_EQ(list.top->down->getBook(), Book("ABE"));
    ASSERT_EQ(list.top->getBook(), Book("ABD"));

    list.clear();
}

TEST(MyDoubleLinkedList, rotate_one_element)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(1);
    list.append(Book("ABC"));
    list.append(Book("ABD"));
    list.append(Book("ABE"));
    list.rotate();

    ASSERT_EQ(list.bottom->getBook(), Book("ABC"));
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABD"));
    ASSERT_EQ(list.top->getBook(), Book("ABE"));

    list.clear();
}

TEST(MyDoubleLinkedList, rotate_all_elements)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(3);
    list.append(Book("ABC"));
    list.append(Book("ABD"));
    list.append(Book("ABE"));
    list.rotate();

    ASSERT_EQ(list.bottom->getBook(), Book("ABE"));
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABD"));
    ASSERT_EQ(list.top->getBook(), Book("ABC"));
}

TEST(MyDoubleLinkedList, rotate_more_elements_than_exists)
{
    MyDoubleLinkedList list = MyDoubleLinkedList(4);
    list.append(Book("ABC"));
    list.append(Book("ABD"));
    list.append(Book("ABE"));
    list.rotate();

    ASSERT_EQ(list.bottom->getBook(), Book("ABE"));
    ASSERT_EQ(list.bottom->up->getBook(), Book("ABD"));
    ASSERT_EQ(list.top->getBook(), Book("ABC"));
}
